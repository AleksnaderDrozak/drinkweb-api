export interface DrinkI {
    id?: number;
    name: string,
    imgUrl: string,
    // details: DrinkDetailsI[];
    // sourceLink: string;
}

export interface DrinkDetailsI {
    header: allowedHeader;
    content: string;
}

export type allowedHeader = "ingredients" | "preparation" | "glassType" | "serveMethod";
