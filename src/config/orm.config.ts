import { Drink } from '../modules/drink/entities/drink.entity';
import { Ingredient } from '../modules/ingredient/enities/ingredient.entity';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
import * as envSetup from 'dotenv';
import { DrinkDetails } from '../modules/drink/entities/drink-details.entity';
import { DrinkIngredient } from '../modules/drink/entities/drink-ingredient.entity';
import { DrinkImage } from '../modules/drink/entities/drink-image.entity';
import { DrinkImageSubscriber } from './../modules/drink/subscribers/drink-image.subscriber';


type DatabaseTypes = 'mysql' | 'mariadb' | 'postgres';

envSetup.config({ silent: process.env.NODE_ENV === 'production' });

export const ormConfig: MysqlConnectionOptions | PostgresConnectionOptions = {
    host: process.env.DATABASE_HOST,
    type: process.env.DATABASE_TYPE as DatabaseTypes,
    port: parseInt(process.env.DATABASE_PORT),
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    entities: [
        Drink,
        DrinkDetails,
        Ingredient,
        DrinkIngredient,
        DrinkImage,
    ],
    synchronize: true,
    subscribers: [
        DrinkImageSubscriber,
    ],
} as PostgresConnectionOptions;