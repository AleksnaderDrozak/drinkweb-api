import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeleteResult } from 'typeorm';
import { DrinkDetails } from '../entities/drink-details.entity';
import { DrinkDetailsInput } from '../inputs/drink-details.input';
import { Drink } from '../entities/drink.entity';

@Injectable()
export class DrinkDetailsService {

    constructor(
        @InjectRepository(DrinkDetails)
        private readonly drinkDetailsRepository: Repository<DrinkDetails>,
    ) { }

    public createMany(drinkDetailsInputs: DrinkDetailsInput[], drink: Drink): Array<Promise<DrinkDetails>> {
        return drinkDetailsInputs.map((drinkDetailsInput) => {
            return this.create(drinkDetailsInput, drink);
        });
    }

    public async create(drinkDetailsInput: DrinkDetailsInput, drink: Drink): Promise<DrinkDetails> {
        const drinkDetails = {
            ...drinkDetailsInput,
            drink,
        };
        return this.drinkDetailsRepository.save(drinkDetails);
    }

    public async delete(drinkDetailsId: number) {
        return await this.drinkDetailsRepository.delete(drinkDetailsId)
            .then((deleteResult: DeleteResult) => deleteResult.affected);
    }
}
