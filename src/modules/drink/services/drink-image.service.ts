import { FileI } from '@interfaces/file.interface';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DrinkImageDto } from '../dtos/drink-image.dto';
import { DrinkImage } from '../entities/drink-image.entity';

const fs = require('fs');

@Injectable()
export class DrinkImageService {

    constructor(
        @InjectRepository(DrinkImage)
        private readonly drinkImageRepository: Repository<DrinkImage>,
    ) {
    }

    public async addDrinkImageFile(drinkIdParam: number, file: FileI) {
        let drinkImage = this.drinkImageRepository.create({
            drinkId: drinkIdParam,
            path: file.filename,
        });
        return drinkImage.save();
    }

    public getDrinkImages(drinkId?: number): Promise<DrinkImageDto[]> {
        let drinkIngredientsPromise: Promise<DrinkImageDto[]> =
            this.drinkImageRepository.find({ where: { drinkId: drinkId } });
        return drinkIngredientsPromise;
    }

    public async deleteImage(id: number): Promise<number> {
        this.drinkImageRepository.findOne(id)
            .then((drinkImage: DrinkImage) => this.drinkImageRepository.remove(drinkImage));
        return 1;
    }

    public async deleteDrinkImageFile(drinkImage: DrinkImage) {
        fs.unlink('./../uploads/' + drinkImage.path, (err) => {
            if (err) throw err;
            console.log('successfully deleted');
        });
    }
}
