import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Drink } from '../entities/drink.entity';
import { Repository } from 'typeorm';
import { DrinkInput } from '../inputs/drink.input';

@Injectable()
export class DrinkService {

    constructor(
        @InjectRepository(Drink)
        private readonly drinkRepository: Repository<Drink>,
    ) {
    }

    public async find(id: number): Promise<Drink> {
        return this.drinkRepository.findOne(id);
    }

    public async findAll(orderBy: string = 'DESC'): Promise<Drink[]> {
        return this.drinkRepository.createQueryBuilder('drink')
            .orderBy('drink.id')
            .getMany();
    }

    public async findFromIngredients(ingredientIds: number[]) {
        return this.drinkRepository.find();
    }

    public async findRecent(): Promise<Drink[]> {
        return this.drinkRepository.createQueryBuilder('drink')
            .orderBy('drink.createdAt', 'DESC')
            .limit(20)
            .getMany();
    }

    public async createDrink(drinkInput: DrinkInput): Promise<Drink> {
        const drink = this.drinkRepository.create(drinkInput);
        return this.drinkRepository.save(drink)
            .then((addedDrink) => {
                let found = this.drinkRepository.findOne(drink.id,
                    { relations: ['details', 'drinkIngredients'] });
                return found;
            }).catch((err) => {
                console.log(err)
                throw err;
            });
    }
}

