import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeleteResult } from 'typeorm';
import { DrinkIngredient } from '../entities/drink-ingredient.entity';

@Injectable()
export class DrinkIngredientService {

    constructor(
        @InjectRepository(DrinkIngredient)
        private readonly drinkIngredientRepository: Repository<DrinkIngredient>,
    ) { }

    public findAll(id?: number) {
        let drinkIngredientsPromise: Promise<DrinkIngredient[]> = id ? this.drinkIngredientRepository.find({ where: { drinkId: id } })
            : this.drinkIngredientRepository.find();

        let ingredients = drinkIngredientsPromise.then((drinkIngredients: DrinkIngredient[]) => {
            return drinkIngredients.map((drinkIngredient: DrinkIngredient) => {
                const ingredient = drinkIngredient.ingredient;
                return ingredient;
            });
        });

        return ingredients;
    }

    public async delete(drinkIngredientId: number) {
        return this.drinkIngredientRepository.delete(drinkIngredientId)
            .then((deleteResult: DeleteResult) => deleteResult.affected);
    }
}
