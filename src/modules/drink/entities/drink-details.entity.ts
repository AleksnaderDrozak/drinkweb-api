import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { DrinkDetailsI, allowedHeader } from '@interfaces/drink.interface';
import { Drink } from './drink.entity';

@Entity()
export class DrinkDetails implements DrinkDetailsI {

    @PrimaryGeneratedColumn()
    id?: number;

    @Column("int")
    drinkId: number;

    @ManyToOne(() => Drink, drink => drink.details, {
        onDelete: "CASCADE"
    })
    @JoinColumn({ name: 'drinkId' })
    drink: Drink;

    @Column("text")
    header: allowedHeader;

    @Column("text")
    content: string;
}