import { DrinkI } from '@interfaces/drink.interface';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { DrinkDetails } from './drink-details.entity';
import { DrinkImage } from './drink-image.entity';
import { DrinkIngredient } from './drink-ingredient.entity';

@Entity()
export class Drink implements DrinkI {

    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    name: string;

    @Column("text")
    sourceLink: string;

    @Column("text")
    imgUrl: string;

    @OneToMany(() => DrinkDetails, details => details.drink, {
        cascade: true,
    })
    details: DrinkDetails[];

    @OneToMany(() => DrinkImage, image => image.drink)
    images: DrinkImage[];

    @OneToMany((type) => DrinkIngredient, (drinkIngredient) => drinkIngredient.drink, { cascade: true, eager: true })
    public drinkIngredients!: DrinkIngredient[];


    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;

    public addDrinkIngredient(drinkIngredient: DrinkIngredient) {
        this.drinkIngredients.push(drinkIngredient);
    }
}