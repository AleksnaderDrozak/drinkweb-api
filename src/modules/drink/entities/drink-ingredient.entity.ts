import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, BaseEntity, JoinTable } from 'typeorm';
import { Drink } from './drink.entity';
import { Ingredient } from '../../ingredient/enities/ingredient.entity';
import { ObjectType, Field, Int } from 'type-graphql';
import { IngredientDto } from '../../ingredient/dtos/ingredient.dto';

@ObjectType()
@Entity()
export class DrinkIngredient extends BaseEntity {

    @PrimaryGeneratedColumn()
    public id!: number;

    @Field(() => Int)
    @Column('int')
    public drinkId!: number;

    @Field(() => Int)
    @Column('int')
    public ingredientId!: number;

    @ManyToOne(type => Drink, {
        onDelete: 'CASCADE'
    })
    public drink: Drink;

    @Field(() => IngredientDto)
    @ManyToOne(type => Ingredient, { eager: true, onDelete: 'CASCADE' })
    @JoinColumn({ name: 'ingredientId' })
    @JoinTable()
    public ingredient: Ingredient;

    @Field(() => Int)
    @Column('int')
    @JoinColumn()
    public qty: number;
}
