import { Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn, Column, BaseEntity } from 'typeorm';
import { Drink } from './drink.entity';

@Entity()
export class DrinkImage extends BaseEntity {

    @PrimaryGeneratedColumn()
    id?: number;

    @Column('int')
    drinkId: number;

    @ManyToOne(() => Drink, drink => drink.images, {
        onDelete: 'SET NULL',
    })
    @JoinColumn({ name: 'drinkId' })
    drink: Drink;

    @Column('bool', {
        default: false
    })
    mainPhoto: boolean;

    @Column('text')
    path: string;
}