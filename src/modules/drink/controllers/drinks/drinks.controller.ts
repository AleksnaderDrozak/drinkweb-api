import { Controller, Get, Param, Post, Body } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';

import { Drink } from '../../entities/drink.entity';

import { DrinkDto } from '../../dtos/drink.dto';
// import { DrinkDetails } from '../../entities/drink-details.entity';

@Controller('drinks')
export class DrinksController {

    constructor(
        @InjectRepository(Drink)
        private readonly drinkRespository: Repository<Drink>,
        // @InjectRepository(DrinkDetails)
        // private readonly drinkDetailsRespository: Repository<DrinkDetails>,
    ) { }

    @Get()
    listAll() {
        return this.drinkRespository.find({ relations: ["details"] });
    }

    @Get('recommended')
    listRecommended() {
        return this.drinkRespository.find();
    }

    @Get(":id")
    find(@Param('id') id: number) {
        return this.drinkRespository.findOne(id);
    }

    @Post()
    async store(@Body() drink: DrinkDto) {
        return this.drinkRespository.save(drink)
            // .then(addedDrink =>
            //     drink.details.forEach(details => this.drinkDetailsRespository.save({ ...details, drink: addedDrink }))
            // )
            .then(() =>
                this.drinkRespository.findOne(drink, { relations: ["details"] })
            );
    }

    @Post("many")
    async storeAll(@Body() drinks: DrinkDto[]) {
        return Promise.all(drinks.map(drink => this.store(drink)));
    }

}
