import { Controller, Post, UseInterceptors, UploadedFile, Param } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { DrinkImageService } from '../../services/drink-image.service';
import { FileI } from '../../../../interfaces/file.interface';

export const myStorage = diskStorage({
    // Specify where to save the file
    destination: (req, file, cb) => {
        cb(null, './../uploads');
    },
    // Specify the file name
    filename: (req, file, cb) => {
        cb(null, 'zdjecia/' + Date.now() + '-' + file.originalname.replace(/ /g, '_'));
    },
});

@Controller('drinks/:drinkId/images')
export class DrinkImageControler {

    constructor(private readonly drinkImageService: DrinkImageService) {
    }

    @Post()
    @UseInterceptors(FileInterceptor('image', {
        storage: myStorage,
    }),
    )
    async uploadImage(@Param('drinkId') drinkId: number, @UploadedFile() image: FileI) {
        const drinkImage = await this.drinkImageService.addDrinkImageFile(drinkId, image);
        console.log(image);
        return drinkImage;
    }
}