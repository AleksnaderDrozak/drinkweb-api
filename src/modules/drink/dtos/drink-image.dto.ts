import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class DrinkImageDto {
    @Field()
    public readonly id?: number;
    @Field()
    public readonly path: string;
    @Field()
    public readonly mainPhoto: boolean;
}