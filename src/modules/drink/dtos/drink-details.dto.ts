import { ObjectType, Field, Int } from 'type-graphql';
import { allowedHeader } from '@interfaces/drink.interface';

@ObjectType()
export class DrinkDetailsDto {
    @Field(() => Int)
    public readonly id?: number;
    @Field()
    public readonly header: allowedHeader;
    @Field()
    public readonly content: string;
    @Field(() => Int)
    public readonly drinkId: number;
}