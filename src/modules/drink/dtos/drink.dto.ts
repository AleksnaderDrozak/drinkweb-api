import { DrinkI } from '@interfaces/drink.interface';
import { ObjectType, Field, Int } from 'type-graphql';
import { IngredientDto } from '../../ingredient/dtos/ingredient.dto';
import { DrinkIngredient } from '../entities/drink-ingredient.entity';

@ObjectType()
export class DrinkDto implements DrinkI {
    @Field(() => Int)
    public readonly id?: number;
    @Field()
    public readonly imgUrl: string;
    @Field()
    public readonly sourceLink: string;
    @Field()
    public readonly name: string;
    @Field()
    public readonly createdAt: Date;
    @Field(() => [IngredientDto], { nullable: true })
    public readonly ingredients?: IngredientDto[];
    @Field(() => [DrinkIngredient], { nullable: true })
    public drinkIngredients!: DrinkIngredient[];


}