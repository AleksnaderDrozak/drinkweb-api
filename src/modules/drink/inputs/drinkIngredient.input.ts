import { Field, InputType, Int } from 'type-graphql';

@InputType()
export class DrinkIngredientInput {
    @Field()
    readonly qty: number;
    @Field(() => Int)
    public readonly ingredientId: number;
}
