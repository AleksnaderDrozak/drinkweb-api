import { Field, InputType } from 'type-graphql';
import { IsUrl, IsString, Length } from 'class-validator';
import { DrinkIngredientInput } from './drinkIngredient.input';
import { DrinkDetailsInput } from './drink-details.input';

@InputType()
export class DrinkInput {
    @Field()
    readonly imgUrl: string;
    @Field()
    @IsUrl()
    public readonly sourceLink: string;
    @Field()
    @IsString()
    @Length(2, 80)
    public readonly name: string;
    @Field(() => [DrinkIngredientInput], { nullable: true })
    public readonly drinkIngredients: DrinkIngredientInput[];
    @Field(() => [DrinkDetailsInput], { nullable: true })
    public readonly drinkDetails: DrinkDetailsInput[];
}