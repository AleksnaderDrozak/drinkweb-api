import { Field, InputType, Int } from 'type-graphql';
import { allowedHeader } from '@interfaces/drink.interface';

@InputType()
export class DrinkDetailsInput {
    @Field(() => Int, { nullable: true })
    public readonly drinkId?: number;
    @Field()
    public readonly header: allowedHeader;
    @Field()
    public readonly content: string;
}