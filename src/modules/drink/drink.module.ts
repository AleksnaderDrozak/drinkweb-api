import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DrinksController } from './controllers/drinks/drinks.controller';
import { DrinkService } from './services/drink.service';
import { Drink } from './entities/drink.entity';
import { DrinkResolver } from './resolvers/drink.resolver';
import { DrinkDetails } from './entities/drink-details.entity';
import { IngredientModule } from './../ingredient/ingredient.module';
import { DrinkIngredient } from './entities/drink-ingredient.entity';
import { DrinkIngredientService } from './services/drinkIngredient.service';
import { DrinkImageControler } from './controllers/drinksImages/drink-image.controller';
import { MulterModule } from '@nestjs/platform-express';
import { DrinkImage } from './entities/drink-image.entity';
import { DrinkImageService } from './services/drink-image.service';
import { DrinkImageResolver } from './resolvers/drink-image.resolver';
import { DrinkDetailsService } from './services/drink-details.service';
import { DrinkDetailsResolver } from './resolvers/drink-details.resolver';
import { DrinkImageSubscriber } from './subscribers/drink-image.subscriber';
// import { Connection } from 'typeorm';

const drinkImageServiceProvider = { provide: 'DRINK_IMAGE_SERVICE', useValue: DrinkImageService };

@Module({
    imports: [IngredientModule,
        TypeOrmModule.forFeature([Drink, DrinkDetails, DrinkIngredient, DrinkImage]),
        MulterModule.register({
            dest: './uploads',
        }),
        //  Connection,
    ],
    controllers: [DrinksController, DrinkImageControler],
    providers: [DrinkService, DrinkDetailsService, DrinkIngredientService, DrinkResolver, DrinkImageService,
        drinkImageServiceProvider, DrinkImageResolver, DrinkDetailsResolver, DrinkImageSubscriber],
    exports: [DrinkImageSubscriber]
})
export class DrinkModule { }
