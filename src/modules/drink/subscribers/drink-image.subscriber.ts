import { EntitySubscriberInterface, RemoveEvent, Connection } from 'typeorm';
import { DrinkImage } from '../entities/drink-image.entity';
import { DrinkImageService } from '../services/drink-image.service';
import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
// import { InjectConnection } from '@nestjs/typeorm';

// @EventSubscriber()
@Injectable()
export class DrinkImageSubscriber implements EntitySubscriberInterface<DrinkImage> {

    constructor(
        @InjectConnection() readonly connection: Connection,
        private readonly drinkImageService: DrinkImageService) {
        connection.subscribers.push(this);
    }

    listenTo() {
        return DrinkImage;
    }

    beforeRemove(event: RemoveEvent<DrinkImage>) {
        console.log(`BEFORE IMAGE REMOVED: `, event.entity);
        this.drinkImageService.deleteDrinkImageFile(event.entity);
    }

}