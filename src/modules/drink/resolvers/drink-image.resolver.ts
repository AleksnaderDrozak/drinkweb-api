import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { Int } from 'type-graphql';
import { DrinkImage } from './../entities/drink-image.entity';
import { DrinkImageService } from './../services/drink-image.service';

@Resolver(of => DrinkImage)
export class DrinkImageResolver {

    constructor(
        private readonly drinkImageService: DrinkImageService,
    ) { }

    @Mutation(() => Int)
    async deleteDrinkImage(@Args({ name: 'drinkImageId', type: () => Int }) drinkImageId: number): Promise<number> {
        return await this.drinkImageService.deleteImage(drinkImageId);
    }
}
