import { DrinkService } from '../services/drink.service';
import { DrinkInput } from '../inputs/drink.input';
import { Resolver, Query, Mutation, Args, ResolveProperty, Parent } from '@nestjs/graphql';
import { Drink } from '../entities/drink.entity';
import { DrinkIngredientService } from '../services/drinkIngredient.service';
import { DrinkImageDto } from '../dtos/drink-image.dto';
import { DrinkImageService } from '../services/drink-image.service';
import { DrinkDetailsService } from '../services/drink-details.service';
import { DrinkDto } from '../dtos/drink.dto';
import { IngredientDto } from './../../../modules/ingredient/dtos/ingredient.dto';
import { Int } from 'type-graphql';

@Resolver(of => DrinkDto)
export class DrinkResolver {

    constructor(
        private readonly drinkService: DrinkService,
        private readonly drinkIngredientService: DrinkIngredientService,
        private readonly drinkImageService: DrinkImageService,
        private readonly drinkDetailsService: DrinkDetailsService,
    ) { }

    @Query(() => [DrinkDto], { 'name': 'drinks' })
    async drinks() {
        let drinks = await this.drinkService.findAll();
        return drinks;
    }

    @Query(() => [DrinkDto])
    async recentDrinks() {
        return await this.drinkService.findRecent();
    }

    @Query(() => [DrinkDto])
    async findFromIngredients(@Args({ name: 'ingredientsIds', type: () => [Int] }) ingredientsIds: number[]) {
        return this.drinkService.findFromIngredients(ingredientsIds)
    }

    @ResolveProperty(() => [IngredientDto], { 'name': 'skladniki' })
    async ingredients(@Parent() drink) {
        const { id } = drink;
        return await this.drinkIngredientService.findAll(id);
    }

    @ResolveProperty(() => [DrinkImageDto])
    async drinkImages(@Parent() drink) {
        const { id: drinkId } = drink;
        return await this.drinkImageService.getDrinkImages(drinkId);
    }

    @Mutation(() => DrinkDto)
    async createDrink(@Args('input') input: DrinkInput) {
        return this.drinkService.createDrink(input).then((drink: Drink) => {
            if (input.drinkDetails) {
                this.drinkDetailsService.createMany(input.drinkDetails, drink);
            }
            return drink;
        });
    }
}