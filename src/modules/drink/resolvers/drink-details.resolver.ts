import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { Int } from 'type-graphql';
import { DrinkDetailsDto } from '../dtos/drink-details.dto';
import { DrinkDetailsInput } from '../inputs/drink-details.input';
import { DrinkDetailsService } from '../services/drink-details.service';
import { DrinkService } from '../services/drink.service';
import { DrinkImage } from './../entities/drink-image.entity';

@Resolver(of => DrinkImage)
export class DrinkDetailsResolver {

    constructor(
        private readonly drinkDetailsService: DrinkDetailsService,
        private readonly drinkService: DrinkService,
    ) { }

    @Mutation(() => Int)
    async deleteDrinkDetails(@Args({ name: 'drinkDetailsId', type: () => Int }) drinkDetailsId: number): Promise<number> {
        return await this.drinkDetailsService.delete(drinkDetailsId);
    }

    @Mutation(() => DrinkDetailsDto)
    async addDrinkDetails(@Args('drinkDetailsInput') drinkDetailsInput: DrinkDetailsInput) {
        const drinkDetails: DrinkDetailsDto = await this.drinkService.find(drinkDetailsInput.drinkId).then((drink) => {
            return this.drinkDetailsService.create(drinkDetailsInput, drink);
        });
        return drinkDetails;
    }
}
