import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { IngredientsService } from './services/ingredients/ingredients.service';
import { IngredientDto } from './dtos/ingredient.dto';
import { IngredientInput } from './inputs/ingredient.input';
import { Int } from 'type-graphql';

@Resolver()
export class IngredientResolver {

    constructor(
        private readonly ingredientsService: IngredientsService,
    ) { }

    @Query(() => IngredientDto)
    async ingredient(@Args('id') id: number) {
        return this.ingredientsService.findOne(id);
    }

    @Query(() => [IngredientDto])
    async ingredients() {
        return this.ingredientsService.findAll();
    }

    @Mutation(() => IngredientDto)
    async createIngredient(@Args('input') input: IngredientInput) {
        return this.ingredientsService.create(input);
    }

    @Mutation(() => IngredientDto)
    async updateIngredient(@Args('input') input: IngredientInput) {
        const res = await this.ingredientsService.update(input, input.id);
        return res;
    }

    @Mutation(() => Int)
    async deleteIngredient(@Args('ingredientId') ingredientId: number) {
        return this.ingredientsService.delete(ingredientId);
    }
}