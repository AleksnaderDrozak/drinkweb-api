import { Injectable } from '@nestjs/common';
import { Ingredient } from '../../enities/ingredient.entity';
import { Repository, DeleteResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { IngredientInput } from 'modules/ingredient/inputs/ingredient.input';
import { first } from "lodash";


@Injectable()
export class IngredientsService {
    constructor(
        @InjectRepository(Ingredient)
        private readonly ingredientReposiotry: Repository<Ingredient>
    ) { }

    public findOne(id: number) {
        return this.ingredientReposiotry.findOne(id);
    }

    public findAll(): Promise<Ingredient[]> {
        return this.ingredientReposiotry.find();
    }

    public async create(ingredientInput: IngredientInput): Promise<Ingredient> {
        return this.ingredientReposiotry.find({ where: { name: ingredientInput.name } })
            .then((existing: Ingredient[]) => first(existing) || this.ingredientReposiotry.save(this.ingredientReposiotry.create(ingredientInput)));
    }

    public async update(ingredientInput: IngredientInput, ingredientId: number) {
        return this.ingredientReposiotry.update(ingredientId, { ...ingredientInput })
            .then(() => this.ingredientReposiotry.findOne(ingredientId));
    }

    public async delete(ingredientId: number) {
        return this.ingredientReposiotry.delete(ingredientId)
            .then((deleteResult: DeleteResult) => deleteResult.affected);
    }
}
