import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { DrinkIngredient } from '../../drink/entities/drink-ingredient.entity';
import { Unit } from '../intefaces/ingredients.models';

@Entity()
export class Ingredient extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({
        type: "enum",
        enum: Unit,
        nullable: true,
    })
    unit: Unit;

    @OneToMany((type) => DrinkIngredient, (drinkIngredient) => drinkIngredient.ingredient)
    public drinkIngredients?: DrinkIngredient[];

}