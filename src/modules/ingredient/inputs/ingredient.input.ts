import { Unit } from '../intefaces/ingredients.models';
import { Field, Int, InputType } from 'type-graphql';

@InputType()
export class IngredientInput {
    @Field(() => Int, { nullable: true })
    readonly id?: number;
    @Field()
    readonly name: string;
    @Field(() => Int)
    readonly unit: Unit;
}
