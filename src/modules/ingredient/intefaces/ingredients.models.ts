export enum Unit {
    ml,
    g,
    piece,
}

export interface Ingredient {
    name: string;
    unit: Unit;
}
