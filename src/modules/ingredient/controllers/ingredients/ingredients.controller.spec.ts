import { Test } from '@nestjs/testing';
import { IngredientsController } from './ingredients.controller';
import { Ingredient } from './../../enities/ingredient.entity';
import { IngredientsService } from './../../services/ingredients/ingredients.service';
import { Unit } from '../../intefaces/ingredients.models';


type ingredientCreatorCallback = (id: number, name: string, unit: Unit) => Ingredient;

let ingredients: Ingredient[];

class IngredientsServiceMock {
  public findAll(): Ingredient[] {

    const createIngredient: ingredientCreatorCallback = (id, name, unit) => Object.assign(new Ingredient(), { name, unit, id });

    ingredients = [
      createIngredient(1, 'gin', Unit.ml),
      createIngredient(2, 'cytryna', Unit.piece),
      createIngredient(3, 'cukier', Unit.g)
    ];

    return ingredients;

  }

}

describe('Ingredients Controller', () => {

  let ingredientsController: IngredientsController;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      controllers: [IngredientsController],
      providers: [{
        provide: IngredientsService,
        useValue: new IngredientsServiceMock(), // or alternatively useClass: DeviceServiceMock
      }],
    }).compile();

    ingredientsController = module.get<IngredientsController>(IngredientsController);
  });

  describe('findAll', () => {
    it('should return all ingredients', async () => {
      expect(await ingredientsController.findAll()).toBe(ingredients);
    });
  });
});
