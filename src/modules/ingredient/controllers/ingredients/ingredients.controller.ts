import { Controller, Get } from '@nestjs/common';
import { Ingredient } from '../../enities/ingredient.entity';
import { IngredientsService } from '../../services/ingredients/ingredients.service';

@Controller('ingredients')
export class IngredientsController {

    constructor(private readonly ingredientsService: IngredientsService) {
    }

    @Get()
    async findAll(): Promise<Ingredient[]> {
        return this.ingredientsService.findAll();
    }
}
