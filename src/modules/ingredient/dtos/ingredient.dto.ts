import { Unit } from '../intefaces/ingredients.models';
import { Field, Int, ObjectType, InputType } from 'type-graphql';

@ObjectType()
@InputType('ingredientInputType')
export class IngredientDto {
    @Field()
    readonly name: string;
    @Field(() => Int, {
        nullable: true
    })
    readonly unit: Unit;
    @Field()
    readonly id?: number;
}
