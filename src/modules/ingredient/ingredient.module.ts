import { Module } from '@nestjs/common';
import { IngredientsService } from './services/ingredients/ingredients.service';
import { IngredientsController } from './controllers/ingredients/ingredients.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Ingredient } from './enities/ingredient.entity';
import { IngredientResolver } from './ingredient.resolver';
import { IngredientDto } from './dtos/ingredient.dto';

@Module({
    imports: [TypeOrmModule.forFeature([
        Ingredient
    ])],
    controllers: [IngredientsController],
    providers: [IngredientsService, IngredientResolver, IngredientDto],
    exports: [IngredientDto, IngredientsService],
})
export class IngredientModule { }
