import { Module } from '@nestjs/common';

import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { ormConfig } from '../../config/orm.config';

import { ConfigModule } from 'nestjs-config';
import { resolve as resolvePath } from "path";

@Module({
    imports: [
        ConfigModule.load(resolvePath(__dirname, 'config', '**/!(*.d).{ts,js}')),
        TypeOrmModule.forRoot(ormConfig as TypeOrmModuleOptions),
    ],
})
export class BootstrapModule { }
