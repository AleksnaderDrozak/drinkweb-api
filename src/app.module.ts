import { Module } from '@nestjs/common';
import { DrinkModule } from './modules/drink/drink.module';
import { BootstrapModule } from './modules/bootstrap/bootstrap.module';
import { IngredientModule } from './modules/ingredient/ingredient.module';
import { GraphQLModule } from '@nestjs/graphql';
import { DrinkIngredient } from './modules/drink/entities/drink-ingredient.entity';
import { FilesModule } from './modules/files/files.module';

@Module({
  imports: [
    BootstrapModule,
    DrinkModule,
    IngredientModule,
    GraphQLModule.forRoot({
      autoSchemaFile: 'drinkweb.gql',
    }
    ),
    FilesModule,
  ],
  exports: [DrinkIngredient],
  providers: [DrinkIngredient]
})
export class AppModule { }
